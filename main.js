/*
 Copyright (c) 2012 Zhiya Zang.
 This demo simulates a timer based on javascript and html5.

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 */

function init() {
    lineWidth = 10;
    lineHeight = 50;
    lineHalf = lineWidth / 2;
    setInterval("displayTime()",1);
}

function displayTime() {
    var time = new Date();
    var hour = time.getHours();
    var dis0 = parseInt(hour / 10);
    var dis1 = parseInt(hour % 10);
    var min = time.getMinutes();
    var dis2 = parseInt(min / 10);
    var dis3 = parseInt(min % 10);
    var sec = time.getSeconds();
    var dis4 = parseInt(sec / 10);
    var dis5 = parseInt(sec % 10);
    var milisec = time.getMilliseconds();
    var dis6 = parseInt(milisec / 100);
    var dis7 = parseInt(milisec % 100 / 10);
    var dis8 = parseInt(milisec % 10);
    displayNum(dis0, 0);
    displayNum(dis1, 1);
    displayNum(dis2, 2);
    displayNum(dis3, 3);
    displayNum(dis4, 4);
    displayNum(dis5, 5);
    displayNum(dis6, 6);
    displayNum(dis7, 7);
    displayNum(dis8, 8);
}

function displayNum(num, canvasId) { 
    drawing = document.getElementById(canvasId);
    drawing.width = 70;
    drawing.height = 130;
    context = drawing.getContext("2d");
    context.beginPath();
    switch(num) {
        case 0:
            displayLine(0,canvasId);
            displayLine(2,canvasId);
            displayLine(10,canvasId);
            displayLine(11,canvasId);
            displayLine(20,canvasId);
            displayLine(21,canvasId);
            break;
        case 1:
            displayLine(11,canvasId);
            displayLine(21,canvasId);
            break;
        case 2:
            displayLine(0,canvasId);
            displayLine(1,canvasId);
            displayLine(2,canvasId);
            displayLine(11,canvasId);
            displayLine(20,canvasId);
            break;
        case 3:
            displayLine(0,canvasId);
            displayLine(1,canvasId);
            displayLine(2,canvasId);
            displayLine(11,canvasId);
            displayLine(21,canvasId);
            break;
        case 4:
            displayLine(1,canvasId);
            displayLine(10,canvasId);
            displayLine(11,canvasId);
            displayLine(21,canvasId);
            break;
        case 5:
            displayLine(0,canvasId);
            displayLine(1,canvasId);
            displayLine(2,canvasId);
            displayLine(10,canvasId);
            displayLine(21,canvasId);
            break;
        case 6:
            displayLine(0,canvasId);
            displayLine(1,canvasId);
            displayLine(2,canvasId);
            displayLine(10,canvasId);
            displayLine(20,canvasId);
            displayLine(21,canvasId);
            break;
        case 7:
            displayLine(0,canvasId);
            displayLine(11,canvasId);
            displayLine(21,canvasId);
            break;
        case 8:
            displayLine(0,canvasId);
            displayLine(1,canvasId);
            displayLine(2,canvasId);
            displayLine(10,canvasId);
            displayLine(11,canvasId);
            displayLine(20,canvasId);
            displayLine(21,canvasId);
            break;
        case 9:
            displayLine(0,canvasId);
            displayLine(1,canvasId);
            displayLine(2,canvasId);
            displayLine(10,canvasId);
            displayLine(11,canvasId);
            displayLine(21,canvasId);
            break;
    }
    context.fill();
    context.stroke();
}

function displayLine(line, canvasId) {
    //window.alert("ge");
    if (line < 3) {
        //window.alert("hello");
        context.moveTo(lineWidth, lineHalf + line * (lineHeight + lineWidth));
        context.lineTo(lineWidth + lineHalf, (lineHeight + lineWidth) * line);
        context.lineTo(lineHeight, (lineHeight + lineWidth) * line);
        context.lineTo(lineHeight + lineHalf, lineHalf + line * (lineHeight + lineWidth));
        context.lineTo(lineHeight, (lineWidth + lineHeight) * line + lineWidth);
        context.lineTo(lineHalf + lineWidth, (lineWidth + lineHeight) * line + lineWidth);
        context.lineTo(lineWidth, lineHalf + line * (lineHeight + lineWidth));
    } else if ( line > 9 && line < 20) {
        line -= 10;
        context.moveTo(lineHalf + line * (lineHalf + lineHeight), lineWidth);
        context.lineTo(line * (lineHalf + lineHeight), lineHalf + lineWidth);
        context.lineTo(line * (lineHalf + lineHeight), lineHalf + lineHeight);
        context.lineTo(lineHalf + line * (lineHalf + lineHeight), lineHeight + lineWidth);
        context.lineTo(line * (lineHalf + lineHeight) + lineWidth, lineHalf + lineHeight);
        context.lineTo(line * (lineHalf + lineHeight) + lineWidth, lineWidth + lineHalf);
        context.lineTo(lineHalf + line * (lineHalf + lineHeight), lineWidth);
    } else {        
        line -= 20;
        context.moveTo(lineHalf + line * (lineHalf + lineHeight), lineWidth + lineHeight + lineWidth);
        context.lineTo(line * (lineHalf + lineHeight), lineHalf + lineWidth + lineHeight + lineWidth);
        context.lineTo(line * (lineHalf + lineHeight), lineHalf + lineHeight + lineHeight + lineWidth);
        context.lineTo(lineHalf + line * (lineHalf + lineHeight), lineHeight + lineWidth + lineWidth + lineHeight);
        context.lineTo(line * (lineHalf + lineHeight) + lineWidth, lineHalf + lineHeight + lineHeight + lineWidth);
        context.lineTo(line * (lineHalf + lineHeight) + lineWidth, lineWidth + lineHalf + lineHeight + lineWidth);
        context.lineTo(lineHalf + line * (lineHalf + lineHeight), lineWidth + lineWidth + lineHeight);
    }
    context.fillStyle = "#232126";
    context.strokeStyle = "#232126";
} 
